import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-server-item',
  templateUrl: './server-item.component.html',
  styleUrls: ['./server-item.component.css']
})
export class ServerItemComponent {
  @Input() serverId: number | undefined;
  @Input() serverName: string | undefined;
  serverOnline = true;

  toggleOnline() {
    this.serverOnline = !this.serverOnline;
  }
}
