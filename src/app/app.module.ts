import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServerComponent } from './server/server.component';
import {FormsModule} from "@angular/forms";
import { ServerItemComponent } from './server-item/server-item.component';

@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    ServerItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
