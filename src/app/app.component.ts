import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'fitstic-angular-2023';
  newServerId = undefined;
  newServerName = "";
  errorMessage: string | undefined = undefined

  serverList = [
    {id: 2, name: "Pippo"},
    {id: 5, name: "Pluto"},
    {id: 10, name: "Paperino"},
  ]

  addNewServer(){
    if (!this.newServerName) {
      this.errorMessage = "Server name can not be empty!"
      return
    }
    const newServer = {
      id: Number(this.newServerId),
      name: this.newServerName
    }
    this.serverList.push(newServer)
    this.newServerId = undefined
    this.newServerName = ""
    this.errorMessage = undefined

  }

  protected readonly undefined = undefined;
}
